FROM crops/poky:ubuntu-20.04

USER root
RUN apt update && apt install python3-pip
RUN pip3 install kas==2.2
USER usersetup
